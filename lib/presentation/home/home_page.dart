import 'package:firebase_maps_app/domain/bloc/login/login_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import './map_widget.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
      builder: (BuildContext context, LoginState state) {
        return Scaffold(
          body: Container(
              width: MediaQuery.of(context).size.width,
              color: Colors.green,
              child: Column(
                children: [
                  Text(
                    "UID: ${state.user.uid}",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "Name: ${state.user.name}",
                    style: TextStyle(fontSize: 20),
                  ),
                  Container(height: 455, color: Colors.red, child: MapWidget()),
                ],
              )),
        );
      },
    );
  }
}
