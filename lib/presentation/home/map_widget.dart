import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapWidget extends StatelessWidget {
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.4220698, -122.0862784),
    zoom: 14.5,
  );
  @override
  Widget build(BuildContext context) {
    return GoogleMap(initialCameraPosition: _kGooglePlex);
  }
}
