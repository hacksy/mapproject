import 'package:firebase_maps_app/domain/bloc/login/login_cubit.dart';
import 'package:firebase_maps_app/presentation/home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'signup/login_anonimo_button.dart';
import 'signup/login_google_button.dart';
import 'signup/login_facebook_button.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Servicio de Login"),
      ),
      body: BlocListener<LoginCubit, LoginState>(
        listener: (BuildContext context, LoginState state) {
          switch (state.status) {
            case LoginStatus.none:
              break;
            case LoginStatus.loading:
              break;
            case LoginStatus.failure:
              var snackbar = SnackBar(
                content: Text("Error iniciando sesion"),
              );
              Scaffold.of(context).showSnackBar(snackbar);
              break;
            case LoginStatus.logged:
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (BuildContext context) {
                return HomePage();
              }));
              break;
          }
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              LoginAnonimoButton(),
              LoginGoogleButton(),
              LoginFacebookButton(),
            ],
          ),
        ),
      ),
    );
  }
}
