import 'package:firebase_maps_app/domain/bloc/login/login_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginGoogleButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RaisedButton.icon(
      label: Text("Iniciar Sesion con Google"),
      icon: Icon(Icons.g_translate),
      onPressed: () {
        BlocProvider.of<LoginCubit>(context).googleLogin();
      },
    );
  }
}
