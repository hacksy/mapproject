part of 'login_cubit.dart';

enum LoginStatus { none, loading, failure, logged }

class LoginState extends Equatable {
  const LoginState._({
    this.status = LoginStatus.none,
    this.user,
  });

  final LoginStatus status;
  final FUser user;

  const LoginState.none() : this._(status: LoginStatus.none);
  const LoginState.loading() : this._(status: LoginStatus.loading);
  const LoginState.failure() : this._(status: LoginStatus.failure);
  const LoginState.logged(user)
      : this._(status: LoginStatus.logged, user: user);

  @override
  List<Object> get props => [user, status];
}
