import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_maps_app/domain/entity/user.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit() : super(LoginState.none());

  void anonymousLogin() async {
    emit(LoginState.loading());
    UserCredential userCredential =
        await FirebaseAuth.instance.signInAnonymously();

    FUser user = FUser(
      name: userCredential.user.displayName ?? '',
      uid: userCredential.user.uid,
    );
    emit(LoginState.logged(user));
  }

  void googleLogin() async {
    emit(LoginState.loading());

    final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    final GoogleAuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    UserCredential userCredential =
        await FirebaseAuth.instance.signInWithCredential(credential);

    FUser user = FUser(
      name: userCredential.user.displayName ?? '',
      uid: userCredential.user.uid,
    );
    emit(LoginState.logged(user));
  }

  void facebookLogin() async {
    emit(LoginState.loading());

    final LoginResult result = await FacebookAuth.instance.login();

   
 final FacebookAuthCredential credential = FacebookAuthProvider.credential(
    result.accessToken.token
    );

    UserCredential userCredential =
        await FirebaseAuth.instance.signInWithCredential(credential);

    FUser user = FUser(
      name: userCredential.user.displayName ?? '',
      uid: userCredential.user.uid,
    );
    emit(LoginState.logged(user));
  }
}
